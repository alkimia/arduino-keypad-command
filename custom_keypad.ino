//www.elegoo.com
//2016.12.9

/* @file CustomKeypad.pde
|| @version 1.2
|| @author Alexander Brevig
|| @author Barrionuevo (JM)
|| @contact pepebarrionuevo.es
||
|| @description
|| | Demonstrates changing the keypad size and key values.
|| | Send commands
|| #
*/
#include <Keypad.h>
#include "pitches.h"
#include <Servo.h>

Servo myservo;  // create servo object to control a servo

const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

int melody[] = {
  NOTE_C5, NOTE_D5, NOTE_E5, NOTE_F5, NOTE_G5, NOTE_A5, NOTE_B5, NOTE_C6};
int duration = 500;  // 500 miliseconds

byte rowPins[ROWS] = {9, 8, 7, 6}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {5, 4, 3, 2}; //connect to the column pinouts of the keypad
byte pinSound = 10;
int pos_servo = 0;    // variable to store the servo position

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

char instr[20];
char command[20];
int idx = 0;
char introKey = '#';
char char_split = '*';
//char token[20];

int ledPin = 12;

void moveServo(int pos_servo){
    myservo.write(pos_servo);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
}

void turnLed(int sta) {
  if (sta == 1) {
    digitalWrite(ledPin, HIGH);
  } else if (sta == 0) {
    digitalWrite(ledPin, LOW);
  }
}

String getToken(char data[20], char separator, int param_offset)
{
  char command[20];
  char param[20];   
  int found = 0;
  int index = 0;
  
  for(int i=0; i<20; i++){
    if (data[i] == separator) {
      found = 1;      
      index = 0;
    } else {
      if (!found) 
      {
        command[index] = data[i];
      } else {
        param[index] = data[i];
      }
      ++index;
    }
    
  }
  if (param_offset == 0) {
    return command;
  } else {
    return param;
  }
}

bool procEntry(char instr[20]) {
  String command = "";
  String param = "";
  Serial.println("procEntry...");

  // separe command - param
  command = getToken(instr, char_split, 0);
  param = getToken(instr, char_split, 1);

  if (command == "AA") {
    Serial.println("Turning led");
    play_ok();
    if (param != "") {
      Serial.println(param);
      turnLed(param.toInt());
    }    
  } else if (command == "ABCD" ) {
    Serial.println("Test ok");
    play_ok();
  } else if (command == "DD") {
    play_ok();
    if (param != "") {
       Serial.println(param);
      moveServo(param.toInt());
    }
  } else {
    Serial.println("Unknown...");
    play_wrong();
  }



  // clear buffer
  idx = 0;
  memset(instr,0,strlen(instr));
}

void play_wrong() {
 tone(pinSound, melody[0], 1000);
}

void play_ok() {
 tone(pinSound, melody[1], 200);
 delay(100);
 tone(pinSound, melody[3], 200);
 delay(100);
 tone(pinSound, melody[5], 200);
}

void play_onkey() {
 tone(pinSound, melody[3], 100);
}

void setup(){
  Serial.begin(9600);
  myservo.attach(11);  // attaches the servo on pin 9 to the servo object
  instr[0] = '\0';
  idx = 0;
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
}
  
void loop(){
  
  char customKey = customKeypad.getKey();  
  if (customKey){
    play_onkey();
    Serial.println(customKey);
    if (customKey == introKey) {
      procEntry(instr);      
    } else {
      instr[idx] = customKey;
      idx = idx + 1;
    }
  }
}
